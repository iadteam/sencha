/*
    This file is generated and updated by Sencha Cmd. You can edit this file as
    needed for your application, but these edits will have to be merged by
    Sencha Cmd when it performs code generation tasks such as generating new
    models, controllers or views and when running "sencha app upgrade".

    Ideally changes to this file would be limited and most work would be done
    in other places (such as Controllers). If Sencha Cmd cannot merge your
    changes and its generated code, it will produce a "merge conflict" that you
    will need to resolve manually.
*/

Ext.application({
    name: 'laboratory',
	// autoCreateViewport: true,
	
    requires: [
        'Ext.MessageBox'
    ],

    views: [
        'Main', 
		'Presets', 'Devices', 'Settings',
		'TabMenu',  'MainMenu', 'ContainerWithMenu',
		'DeviceOverview', 
		'VideoList', 'ScreenList' , 'AudioList', 'InputList'
    ],
	controllers: [
		'MainController'
	],

    icon: {
        '57': 'resources/icons/Icon.png',
        '72': 'resources/icons/Icon~ipad.png',
        '114': 'resources/icons/Icon@2x.png',
        '144': 'resources/icons/Icon~ipad@2x.png'
    },

    isIconPrecomposed: true,

    startupImage: {
        '320x460': 'resources/startup/320x460.jpg',
        '640x920': 'resources/startup/640x920.png',
        '768x1004': 'resources/startup/768x1004.png',
        '748x1024': 'resources/startup/748x1024.png',
        '1536x2008': 'resources/startup/1536x2008.png',
        '1496x2048': 'resources/startup/1496x2048.png'
    },

    launch: function() {
        // Destroy the #appLoadingIndicator element
        Ext.fly('appLoadingIndicator').destroy();

		var main = {
            xtype: "main"
        };
		var devices = {
            xtype: "devices"
        };
		var settings = {
            xtype: "settings"
        };
        // Initialize the main view
        Ext.Viewport.add([main,devices,settings]);
		
		/* overlay = Ext.Viewport.add({
            xtype: 'panel',

            // We give it a left and top property to make it floating by default
            rigth: '5%',

            // Make it modal so you can click the mask to hide the overlay
            modal: true,
            hideOnMaskTap: true,

            // Make it hidden by default
            hidden: true,

            // Set the width and height of the panel
            width: '50%',
            height: '100%',

            // Here we specify the #id of the element we created in `index.html`
            // contentEl: 'content',
			html : 'lorem',
			
			// Style the content and make it scrollable
            styleHtmlContent: true,
            scrollable: true,

            // Insert a title docked at the top with a title
            items: [
                {
                    docked: 'top',
                    xtype: 'toolbar',
                    title: 'Overlay Title'
                }
            ]
        }); */

        // Add a new listener onto the viewport with a delegate of the `button` xtype. This adds a listener onto every
        // button within the Viewport, which includes the buttons we added in the toolbar above.
        /* Ext.Viewport.on({
            delegate: 'button',
            tap: function(button) {
                // When you tap on a button, we want to show the overlay by the button we just tapped.
                overlay.showBy(button);
            }
        }); */
    },

    onUpdated: function() {
        Ext.Msg.confirm(
            "Application Update",
            "This application has just successfully been updated to the latest version. Reload now?",
            function(buttonId) {
                if (buttonId === 'yes') {
                    window.location.reload();
                }
            }
        );
    }
});
