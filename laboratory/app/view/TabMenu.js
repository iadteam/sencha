Ext.define('laboratory.view.TabMenu', {
	extend: 'Ext.tab.Panel',
	
	config: {
		ui: 'dark',
		tabBar: {
			ui: 'dark',
			layout: {
				park: Ext.filterPlatform('ie10') ? 'start' : 'center'
			}
		},
		activeTab: 1,
		defaults: {
			scrollable: true
		},
		items: [
			{
				title: 'Choose Devices',
				html: 'Choose your Devices',
				cls: 'card dark',
				iconCls: Ext.theme.is.Blackberry || Ext.theme.is.CupertinoClassic || Ext.theme.is.Tizen ? 'home' : null
			},
			{
				title: 'Set Layout',
				html: 'Set Layout Position',
				cls: 'card dark',
				iconCls: Ext.theme.is.Blackberry || Ext.theme.is.CupertinoClassic || Ext.theme.is.Tizen ? 'home' : null
			}
		]
			
	}
});