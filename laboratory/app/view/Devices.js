Ext.define('laboratory.view.Devices', {
	extend: 'Ext.Container',
	alias: "widget.devices",
	id: 'id-devices',
	requires: [
		'Ext.SegmentedButton'
	],
	
	config: {
		cls: 'card',
		items: [
			{
				xtype: 'toolbar',
				ui: 'light',
				docked: 'top',
				scrollable: {
					direction : 'horizontal',
					indicators: false
				},
				items: [
					{
						text: 'Back',
						ui: 'back',
						hidden: false
					},
					{
						xtype: 'spacer'
					},
					{
						xtype: 'segmentedbutton',
						allowDepress: true,
						items: [
							{
								text: 'Choose Devices',
								pressed: true,
								handler: function() {
									var panel = Ext.create('Ext.Panel', {
									floating: true,
									centered: true,
									width: 200,
									height: 200,
									html: 'LIST DEVICES HERE!'
									});
									Ext.Viewport.add(panel);
									panel.show();
								}
							},
							{
								text: 'Set Layout',
								handler: function() {
									var panel = Ext.create('Ext.Panel', {
									floating: true,
									centered: true,
									width: 200,
									height: 200,
									html: 'TAKE CARE ABOUT SETTING LAYOUT HERE!'
									});
									Ext.Viewport.add(panel);
									panel.show();
								}
							}
						]
					},
					{
						xtype: 'spacer'
					},
					{
						text: 'Save',
						ui: 'action',
						hidden: false
					}
				]
			}
		]
	},
	
	constructor: function() {
		this.on({
			scope: this,
			delegate: 'button',
			tap: 'tapHandler'
		});
		this.callParent(arguments);
	},
	tapHandler: function(button) {
		this.setHtml("<span class=action>User tapped "+button.getText()+"</span>");
	}
});