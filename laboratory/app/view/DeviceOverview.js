Ext.define('laboratory.view.DeviceOverview', {
	extend: 'Ext.Container',
	alias: "widget.deviceoverview",
	requires: [
		'Ext.List',
		'Ext.data.Store',
	],
	config: {
		layout: 'hbox',
		defaults: {
			flex: 1,
		},
		items: [
			{ 
				xtype: 'videolist',
			},
			{ 
				xtype: 'screenlist',
			},
			{ 
				xtype: 'audiolist',
			},
			{ 
				xtype: 'inputlist',
			},
		]
		
		
		
	}
});