Ext.define('laboratory.view.ContainerWithMenu', {
    extend: 'Ext.Container',
	alias: "widget.containerwithmenu",

    requires: [
        'Ext.Menu'
    ],
	
	doSetHidden: function(hidden) {
		this.callParent(arguments);
		
		if(hidden) {
			// Ext.Viewport.removeMenu('left');
		} else {
			var leftSideMenu =  Ext.create('widget.mainmenu', {});
			Ext.Viewport.setMenu(leftSideMenu, {
				side: 'left',
				region: 'west',
				reveal: true,
				// cover: true,
			});
		}
	},

	/* menuForSide: function(side) {
		return Ext.create('widget.mainmenu', {
		});
	}	 */
});