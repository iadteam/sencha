Ext.define('laboratory.view.Settings', {
	extend: 'Ext.Panel',
	alias: "widget.settings",
	id: 'id-settings',
	requires: [
		'Ext.field.Slider',
		'Ext.field.Toggle',
		'Ext.form.FieldSet'
	],	
	config: {
		items: [
			{
                    docked: 'top',
                    xtype: 'toolbar',
                    title: 'Sound Management'
            },
			{
				xtype: 'fieldset',
				defaults: {
					labelWidth: '35%',
					labelAlign: 'top'
				},
				items: [
					{
						xtype: 'sliderfield',
						name: 'thumb',
						value: 20,
						label: 'PC Sound Level'
					},
					{
						xtype: 'sliderfield',
						name: 'thumb',
						value: 20,
						label: 'TV Sound Level'
					},
					{
						xtype: 'sliderfield',
						name: 'thumb',
						value: 20,
						label: 'Master Sound Level'
					}
				]
			}
		]
	}
});