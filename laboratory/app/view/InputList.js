Ext.define('laboratory.view.InputList', {
	extend: 'Ext.Panel',
	alias: "widget.inputlist",
	requires: [
	],
	config: {
		layout: 'vbox',
		defaults: {
			xtype: 'button',
		},
		items: [
			{
				xtype: 'toolbar',
				title: 'Input',
			},
			{
				text: 'Mouse',
				height: 100
			},
			{ 
				text: 'Keyboard',
				height: 100
			},
			{ 
				text: 'Gamepad',
				disabled : true,
				height: 100
			},
			{
				xtype: 'spacer'
			},
		]
	}
});