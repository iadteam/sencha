Ext.define('laboratory.view.AudioList', {
	extend: 'Ext.Panel',
	alias: "widget.audiolist",
	requires: [
	],
	config: {
		layout: 'vbox',
		defaults: {
			xtype: 'button',
		},
		items: [
			{
				xtype: 'toolbar',
				title: 'Audio',
			},
			{
				text: 'Micro PC',
				height: 100
			},
			{ 
				text: 'Micro TV',
				disabled: true,
				height: 100
			},
			{
				xtype: 'spacer'
			},
			{
				xtype: 'spacer'
			},
		]
	}
});