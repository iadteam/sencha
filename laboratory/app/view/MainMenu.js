Ext.define("laboratory.view.MainMenu", {
	extend: "Ext.Menu",
	alias: "widget.mainmenu",
	// id: 'container',
	initialize: function () {
		this.callParent(arguments);
		
		var side = 'left';
		this.setDefaults( {
			xtype: 'button',
			iconCls: 'settings',
			scope: this,
			handler: function() {
				// console.log("hideSideMenuByButton");
				// Ext.Viewport.hideMenu(side);
			}
		});
		this.setItems( [
			{
				text: 'Presets',
				action: 'handlePresets'
			},
			{
				text: 'Devices',
				action: 'handleDevices'
				/* handler: function() {
					var viewPort = Ext.ComponentQuery.query('viewport')[0];
					var p = Ext.widget('dev');
					viewPort.layout.centerRegion.removeAll();
					viewPort.layout.centerRegion.add(p);
					p.show();
					var p = Ext.create('laboratory.view.Devices');
					Ext.Viewport.add(p,{region: 'east'});
					p.show();
				}
				*/
			},
			{
				text: 'Play',
				action: 'handlePlay'
			},
			{
				text: 'Save',
				action: 'handleSave'
			},
			{
				text: 'Sound',
				action: 'handleSettings',
				/* handler: function() {
					Ext.Viewport.hideMenu(side);
					var p = Ext.create('laboratory.view.Settings');
					Ext.Viewport.add(p,{region: 'east'});
					p.show();
				} */
			}
		]);
	}
});