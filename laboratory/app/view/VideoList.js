Ext.define('laboratory.view.VideoList', {
	extend: 'Ext.Panel',
	alias: "widget.videolist",
	requires: [
	],
	config: {
		layout: 'vbox',
		defaults: {
			xtype: 'button',
		},
		items: [
			{
				xtype: 'toolbar',
				title: 'Video',
			},
			{
				text: 'DOM CAM 1',
				height: 100
			},
			{ 
				text: 'DOM CAM 2',
				disabled : true,
				height: 100
			},
			{ 
				text: 'Web Cam 1',
				height: 100
			},
			{ 
				text: 'Web Cam 2',
				disabled : true,
				height: 100
			},
		]
	}
});