Ext.define('laboratory.view.ScreenList', {
	extend: 'Ext.Panel',
	alias: "widget.screenlist",
	requires: [
	],
	config: {
		layout: 'vbox',
		defaults: {
			xtype: 'button',
		},
		items: [
			{
				xtype: 'toolbar',
				title: 'Screen',
			},
			{
				text: 'PC',
				height: 100
			},
			{ 
				text: 'TV',
				disabled : true,
				height: 100
			},
			{
				xtype: 'spacer'
			},
			{
				xtype: 'spacer'
			},
		]
	}
});