Ext.define('laboratory.controller.MainController', {
	extend: 'Ext.app.Controller',
	config: {
		refs: {
			refMain: "main",
			refDevices: "devices",
			refSettings: "settings"
		},
		control: {
			'button[action=handlePresets]': {
				tap: 'handlePresets'
			},
			'button[action=handleSettings]': {
				tap: 'handleSettings'
			},
			'button[action=handlePlay]': {
				tap: 'handlePlay'
			},
			'button[action=handleSave]': {
				tap: 'handleSave'
			},
			'button[action=handleDevices]': {
				tap: 'handleDevices'
			}
		},
		routes: {}
	},
	// helper
	render: function(xtype) {
		console.log("render " + xtype);
        // try to get the view on the viewport, or create it
        var view = Ext.Viewport.child(xtype) || Ext.Viewport.add({ xtype: xtype });
         
        // if is inner, activate
        if (view.isInnerItem()) {
			console.log("render " + xtype + " setActiveItem");
            Ext.Viewport.setActiveItem(view);
        }
         
        // if is floating, just show
        else {
			console.log("render " + xtype + " show");
            view.show();
        }
         
        // return the instance so the controller can
        // make further view updates
        return view;
    },
	slideRightTransition: { type: 'slide', direction: 'right' },
	/* swipeTheMenu: function(e) {
		if(e.direction === "right") {
			Ext.Viewport.toggleMenu('left');
		}
		if(e.direction === "left") {
			Ext.Viewport.hideMenu('left');
		}
	}, */
	onSwipe: function (event) {
         console.log(event.direction);
    },
	hideOverlays : function () {
		var overlay = Ext.ComponentQuery.query('#id-overlaysettings')[0];
		if(overlay)
		{
			overlay.hide();
		}
		overlay = Ext.ComponentQuery.query('#id-overlaydevices')[0];
		if(overlay)
		{
			overlay.hide();
		}
		overlay = Ext.ComponentQuery.query('#id-overlaypresets')[0];
		if(overlay)
		{
			overlay.hide();
		}
	},
	showFloatingPanel: function(panelName) {
		/* var panel = Ext.create('Ext.Panel', {
			floating: true,
			centered: true,
			width: 200,
			height: 200,
			html: htmlText
		});
		Ext.Viewport.add(panel);
		panel.show(); */
		
		this.hideOverlays();
		
		var idName = 'id-overlay' + panelName;
		var lastoverlay = Ext.ComponentQuery.query('#' + idName)[0];
		if(lastoverlay)
		{	
			console.log("reload last overlay");
			lastoverlay.show();
			
			 //console.log("remove last overlay");
			// Ext.Viewport.remove(lastoverlay);
		}
		else{
			var overlay = Ext.Viewport.add({
				xtype: 'panel',
				id : idName,
				// We give it a left and top property to make it floating by default
				rigth: 100,

				// Make it modal so you can click the mask to hide the overlay
				modal: true,
				hideOnMaskTap: true,

				// Make it hidden by default
				hidden: true,

				// Set the width and height of the panel
				width: '70%',
				height: '100%',

				// Here we specify the #id of the element we created in `index.html`
				// contentEl: 'content',
				// html : 'lorem',
				
				// Style the content and make it scrollable
				// styleHtmlContent: true,
				// scrollable: true,

				// Insert a title docked at the top with a title
				items: [
					{
						docked: 'top',
						xtype: panelName
					}
				],
				listeners: {
					element: 'element',
					swipe: function(el) {
						if(el.direction === "left")
						{
							this.hide();
						}
					}
				}
			});
			overlay.show();
		}
		
	},
	// base class func
	init: function() {},
	launch: function() {
		/* this.getRefMain().addListener({ 
			swipe: function(el, node, options) {
				console.log(el.direction);
				},
			scope: this
		}); */
		/* var foo = this.getRefMain();
		foo.on({
			swipe: function(el, node, options) {
				if(el.direction === "right") {
					Ext.Viewport.toggleMenu('left');
				}				
			}
		}); */
		/* this.getRefDevices().on({
			swipe: function(el, node, options) {
				if(el.direction === "right") {
					Ext.Viewport.toggleMenu('left');
				}				
			}
		});
		this.getRefSettings().on({
			swipe: function(el, node, options) {
				if(el.direction === "right") {
					Ext.Viewport.toggleMenu('left');
				}				
			}
		}); */
		Ext.ComponentQuery.query('#id-main')[0].element.on({
			swipe: function(el, node, options) {
				if(el.direction === "right") {
					console.log('main swipe right');
					if(Ext.Viewport.getMenus().left.isHidden())
					{
						Ext.Viewport.showMenu('left');
					}
					// TODO: this.hideOverlays();
				}
			}
		});
		Ext.ComponentQuery.query('#id-devices')[0].element.on({
			swipe: function(el, node, options) {
				if(el.direction === "right") {
					console.log('devices swipe left');
					Ext.Viewport.toggleMenu('left');
				}				
			}
		});
		Ext.ComponentQuery.query('#id-settings')[0].element.on({
			swipe: function(el, node, options) {
				if(el.direction === "right") {
					console.log('settings swipe left');
					Ext.Viewport.toggleMenu('left');
				}				
			}
		});
	},
	// commands
	handlePresets: function() {
		this.showFloatingPanel('presets');
		// this.render('main');
	},
	
	handleSettings: function() {
		this.showFloatingPanel('settings');
		//Ext.Viewport.setActiveItem({xtype:'settings'});
		/* var settings = this.getRefSettings();
		Ext.Viewport.setActiveItem(settings); //settings.show(); */
	},
	
	handlePlay: function() {	
		// this.showFloatingPanel('PLAY!');
		// this.render('main');
		/* var main = this.getRefMain();
		Ext.Viewport.setActiveItem(main); // main.show(); */
		// Ext.Viewport.setActiveItem({xtype:'main'});
	},
	
	handleDevices: function() {
		this.showFloatingPanel('devices');
		// this.render('devices');
		/* var devices = this.getRefDevices();
		devices.show();
		Ext.Viewport.setActiveItem(devices); */
		//Ext.Viewport.animateActiveItem(devices, this.slideRightTransition);
		// Ext.Viewport.setActiveItem({xtype:'devices'});
	},
	
	handleSave: function() {
		// this.showFloatingPanel('SAVE!');
		//this.render('main');
		/* Ext.Viewport.remove(Ext.Viewport.getActiveItem(), true);
		var main = this.getRefMain();
		Ext.Viewport.setActiveItem(main); */
		// Ext.Viewport.setActiveItem({xtype:'main'});
	}
});