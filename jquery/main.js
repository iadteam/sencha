$(document).ready(function() {	$("#panel-main").panel("open");
	$("#main").on("swiperight",function() {
		$("#panel-main").panel("open");
	});
	$("#presets").on("swiperight",function() {
		$("#panel-presets").panel("open");
	});
	$("#choose-devices").on("swiperight",function() {
		$("#panel-choose-devices").panel("open");
	});
	$("#devices").on("swiperight",function() {
		$("#panel-devices").panel("open");
	});
	$("#sound").on("swiperight",function() {
		$("#panel-sound").panel("open");
	});	
	$("#devices .ui-grid-c .ui-btn").click(function() {
		var imgname = $(this).attr('class').match('ui-icon-([a-z0-9]+)')[1];
		if($(this).hasClass("ui-btn-active")) {
			$(this).removeClass("ui-btn-active");
			$(".drag-"+imgname).remove();						if(imgname == getBackgroundImageName())			{				// remove image from background				$("#main").css('background-image', 'none');			}
		} else {
			$(this).addClass("ui-btn-active");
			addImgToMain(imgname);
		}
	});
	
	$("#choose-devices .ui-grid-c .ui-btn").click(function() {
		$(this).toggleClass("ui-btn-active");
	});
	
	$(".save-devices").click(function() {
		$("#choose-devices .ui-grid-c .ui-btn-active").each(function() {
			if($(this).hasClass("ui-btn-active")) {
				var imgname = $(this).attr('class').match('ui-icon-([a-z0-9]+)')[1];
				addImgToMain(imgname);
			}
		});	
	});		$("#main .ui-content").delegate("img", "taphold", function() {		// console.log("img element dblclick event");		$(this).resizable({ containment: "#main", handles: "ne, se, sw, nw" });	});	$("#main .ui-content").delegate(".img-toolbar .ui-icon-delete", "click", function() {		var imgname = $(this).html();		$(".drag-"+imgname).remove();	});		$("#main .ui-content").delegate(".img-toolbar .ui-icon-set-background", "click" , function() {			// bring background to front				var bgname = getBackgroundImageName();		if(bgname)		{			addImgToMain(bgname);		}				// set image as background		var imgname = $(this).html();		$("#main").css('background-image', 'url(images/streams/'+imgname+'.png)');				$(".drag-"+imgname).remove();	});
});	function addImgToMain(imgname) {	$("#main .ui-content").append('<div class="drag-'+imgname+'" style="display:inline-block;"><div class="img-toolbar">' + 									'<div data-role="controlgroup" data-type="horizontal" class="ui-mini ui-btn-left">' +									'<a href="#" class="ui-shadow ui-btn ui-corner-all ui-icon-set-background ui-btn-icon-notext ui-btn-inline">'+imgname+'</a>' +																		'<a href="#" class="ui-shadow ui-btn ui-corner-all ui-icon-transparent ui-btn-icon-notext ui-btn-inline">'+imgname+'</a>' +									'<a href="#" class="ui-shadow ui-btn ui-corner-all ui-icon-delete ui-btn-icon-notext ui-btn-inline">'+imgname+'</a>' +									'</div></div><img src="images/streams/'+imgname+'.png" height="300"/></div>');	//$(".drag."+imgname+" img").resizable({ containment: "#main", handles: "ne, se, sw, nw" });	$(".drag-"+imgname).draggable({ containment: "#main" });}function getBackgroundImageName() {	if( $("#main").css('background-image') === null )	{		return null;	}	var backgroundurl = $("#main").css('background-image');	if(backgroundurl === null)	{		return null;	}	var bgmatch = backgroundurl.match('([a-z0-9]+).png');	if(bgmatch === null)	{		return null;	}	else	{		return bgmatch[1];	}}